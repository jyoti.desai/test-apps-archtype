Feature:Login scenario for BCS Application

  Background:
    * call read 'this:locators.json'
    * def bcsHome = baseUrl + 'bcs/ui/faces/jsps/bcshome.jspx'

  Scenario: Login to BCS homepage
      Given driver bcsHome
       And input(loginPage.username,appUsername)
       And input(loginPage.password,appPassword)
       When submit().click(loginPage.logon)
       Then waitForUrl(bcsHome)
